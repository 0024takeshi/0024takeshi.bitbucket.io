// to get current year
function getYear() {
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    document.querySelector("#displayYear").innerHTML = currentYear;
}

getYear();


// client section owl carousel
$(".client_owl-carousel").owlCarousel({
    loop: true,
    margin: 20,
    dots: false,
    nav: true,
    navText: [],
    autoplay: true,
    autoplayHoverPause: true,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 2
        }
    }
});



/** google_map js **/
function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(40.712775, -74.005973),
        zoom: 18,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}

/** Added by Takeshi  **/
/** Dropdonw list **/
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function dropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
}

/* loading researcheres */
function load_researchers(location) {
    $.getJSON("data/researcher.json" , function(data) {
      var tgt = $("#researcher_list");
      tgt.empty();
      var len = data.length;
      
      for(var i = 0; i < len; i++) {
        if (data[i].location00 == location || location == "all") {
            var clm = $('<div class="col-lg-3 col-sm-6">');
            var box = $('<div class="box">');
            clm.append(box);

            var img = $('<div class="img-box">').append($('<img>').attr('src', data[i].image).addClass('img1').attr('alt', ''));
            box.append(img);

            var detail = $('<div class="detail-box">');
            detail.append($('<h5>').text(data[i].name));
            detail.append($('<p>').html(data[i].comment));
            detail.append($('<p>').html('所属：' + data[i].affiliation + "<br>専門：" + data[i].research_interests.join(', ')));
            if (data[i].hasOwnProperty('url')) { detail.append($('<a>').attr('href', data[i].url).text(data[i].url_title)); }
            box.append(detail);

            tgt.append(clm);
        }
      }
    });
}
